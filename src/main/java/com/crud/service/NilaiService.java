package com.crud.service;

import java.util.List;
import java.util.Optional;

import com.crud.model.transaksi.Nilai;
import com.crud.model.view.ViewNilai;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * NilaiService
 */
public interface NilaiService {

  List<Nilai> getAll();

  List<ViewNilai> getAllView();

  Page<Nilai> getAllPaging(Pageable pageable);

  Optional<Nilai> getById(Long id);

  Nilai save(Nilai nilai);

  Nilai update(Nilai nilai);

  Nilai delete(Long id);

}