package com.crud.service;

import java.util.List;
import java.util.Optional;

import com.crud.model.transaksi.DosenPembimbing;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * DosenPembimbingService
 */
public interface DosenPembimbingService {

  Page<DosenPembimbing> getAllPagingPage(Pageable pageable);

  List<DosenPembimbing> getAll();

  Optional<DosenPembimbing> getById(Long id);

  DosenPembimbing save(DosenPembimbing dosenPembimbing);

  DosenPembimbing update(DosenPembimbing dosenPembimbing);

  DosenPembimbing delete(Long id);
}