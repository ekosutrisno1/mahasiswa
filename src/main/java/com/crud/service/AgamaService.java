package com.crud.service;

import com.crud.model.master.Agama;

import java.util.List;
import java.util.Optional;

public interface AgamaService {
   List<Agama> getAll();

   Optional<Agama> getById(Long id);
}
