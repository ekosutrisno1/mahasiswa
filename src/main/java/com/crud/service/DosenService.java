package com.crud.service;

import java.util.List;
import java.util.Optional;

import com.crud.model.transaksi.Dosen;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * DosenService
 */
public interface DosenService {

  List<Dosen> getAll();

  Page<Dosen> getPaging(Pageable pageable);

  Optional<Dosen> getById(Long id);

  Dosen save(Dosen dosen);

  Dosen update(Dosen dosen);

  Dosen delete(Long id);

}