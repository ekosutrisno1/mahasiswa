package com.crud.service.implement;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.crud.model.transaksi.Nilai;
import com.crud.model.view.ViewNilai;
import com.crud.repository.NilaiRepository;
import com.crud.repository.viewrepo.ViewRepository;
import com.crud.service.NilaiService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * NilaiServiceImpl
 */
@Service
@Transactional
public class NilaiServiceImpl implements NilaiService {

  @Autowired
  private NilaiRepository nilaiRepo;

  @Autowired
  private ViewRepository viewRepo;

  @Override
  public List<Nilai> getAll() {
    List<Nilai> dataNilai = new ArrayList<>();
    for (Nilai nilai : nilaiRepo.findAll()) {
      if (!nilai.getIsDelete()) {
        dataNilai.add(nilai);
      }
    }
    return dataNilai;
  }

  @Override
  public List<ViewNilai> getAllView() {
    List<ViewNilai> dataNilai = new ArrayList<>();
    for (ViewNilai nilai : viewRepo.getAllNilaiView()) {
      dataNilai.add(nilai);
    }
    return dataNilai;
  }

  @Override
  public Page<Nilai> getAllPaging(Pageable pageable) {
    return nilaiRepo.pagination(pageable);
  }

  @Override
  public Optional<Nilai> getById(Long id) {
    return nilaiRepo.findById(id);
  }

  @Override
  public Nilai save(Nilai nilai) {
    nilai.setCreatedBy(1L);
    nilai.setCreatedOn(new Date());
    return nilaiRepo.save(nilai);

  }

  @Override
  public Nilai update(Nilai nilai) {
    Nilai dataNilai = nilaiRepo.findById(nilai.getId()).get();

    dataNilai.setId(nilai.getId());
    dataNilai.setKodeMahasiswa(nilai.getKodeMahasiswa());
    dataNilai.setKodeUjian(nilai.getKodeUjian());
    dataNilai.setNilaiMahasiswa(nilai.getNilaiMahasiswa());

    dataNilai.setModifedBy(02L);
    dataNilai.setCreatedOn(new Date());
    return nilaiRepo.save(dataNilai);
  }

  @Override
  public Nilai delete(Long id) {
    Nilai dataNilai = nilaiRepo.findById(id).get();

    dataNilai.setDeletedBy(04L);
    dataNilai.setDeletedOn(new Date());
    dataNilai.setIsDelete(true);

    return nilaiRepo.save(dataNilai);
  }

}