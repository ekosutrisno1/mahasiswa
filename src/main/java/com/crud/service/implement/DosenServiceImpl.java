package com.crud.service.implement;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.crud.model.transaksi.Dosen;
import com.crud.repository.DosenRepository;
import com.crud.service.DosenService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * DosenServiceImpl
 */
@Service
public class DosenServiceImpl implements DosenService {

  @Autowired
  private DosenRepository dosenRepository;

  @Override
  public List<Dosen> getAll() {
    List<Dosen> dataDosen = new ArrayList<>();
    for (Dosen dosen : dosenRepository.findAll()) {
      if (!dosen.getIsDelete())
        dataDosen.add(dosen);
    }
    return dataDosen;
  }

  @Override
  public Page<Dosen> getPaging(Pageable pageable) {
    return dosenRepository.getPaging(pageable);
  }

  @Override
  public Optional<Dosen> getById(Long id) {
    return dosenRepository.findById(id);
  }

  @Override
  public Dosen save(Dosen dosen) {
    dosen.setCreatedBy(1l);
    dosen.setCreatedOn(new Date());
    return dosenRepository.save(dosen);

  }

  @Override
  public Dosen update(Dosen dosen) {
    Dosen dataDosen = dosenRepository.findById(dosen.getId()).get();

    dataDosen.setNamaDosen(dosen.getNamaDosen());
    dataDosen.setKodeDosen(dosen.getKodeDosen());
    dataDosen.setKodeJurusan(dosen.getKodeJurusan());
    dataDosen.setKodeTypeDosen(dosen.getKodeTypeDosen());

    dataDosen.setModifedBy(1L);
    dataDosen.setModifedOn(new Date());
    return dosenRepository.save(dataDosen);
  }

  @Override
  public Dosen delete(Long id) {
    Dosen dataDosen = dosenRepository.findById(id).get();
    dataDosen.setIsDelete(true);
    dataDosen.setDeletedBy(2L);
    dataDosen.setDeletedOn(new Date());
    return dosenRepository.save(dataDosen);
  }

}