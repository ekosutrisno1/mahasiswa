package com.crud.service.implement;

import com.crud.model.master.Agama;
import com.crud.repository.AgamaRepository;
import com.crud.service.AgamaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AgamaServiceImpl implements AgamaService {
   @Autowired
   AgamaRepository agamaRepository;

   @Override
   public List<Agama> getAll() {
      return new ArrayList<>(agamaRepository.findAll());
   }

   @Override
   public Optional<Agama> getById(Long id) {
      return agamaRepository.findById(id);
   }
}
