package com.crud.service.implement;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.crud.model.transaksi.Mahasiswa;
import com.crud.repository.MahasiswaRepository;
import com.crud.service.MahasiswaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * MahasiswaServiceImpl
 */
@Service
@Transactional
public class MahasiswaServiceImpl implements MahasiswaService {
  @Autowired
  private MahasiswaRepository mhsRepo;

  @Override
  public List<Mahasiswa> getAllMahasiswa() {
    List<Mahasiswa> mahasiswa = new ArrayList<>();
    for (Mahasiswa mhs : mhsRepo.findAll()) {
      if (!mhs.getIsDelete())
        mahasiswa.add(mhs);
    }
    return mahasiswa;
  }

  @Override
  public Optional<Mahasiswa> getMahasiswaById(Long id) {
    return mhsRepo.findById(id);
  }

  @Override
  public Mahasiswa saveMahasiswa(Mahasiswa mahasiswa) {
    mahasiswa.setCreatedOn(new Date());
    mahasiswa.setCreatedBy(1L);
    return mhsRepo.save(mahasiswa);
  }

  @Override
  public Mahasiswa updateMahasiswa(Mahasiswa mahasiswa) {
    Mahasiswa dataMahasiswa = mhsRepo.findById(mahasiswa.getId()).get();
    dataMahasiswa.setId(mahasiswa.getId());
    dataMahasiswa.setNamaMahasiswa(mahasiswa.getNamaMahasiswa());
    dataMahasiswa.setKodeMahasiswa(mahasiswa.getKodeMahasiswa());
    dataMahasiswa.setAlamatMahasiswa(mahasiswa.getAlamatMahasiswa());
    dataMahasiswa.setKodeJurusan(mahasiswa.getKodeJurusan());
    dataMahasiswa.setKodeAgama(mahasiswa.getKodeAgama());
    dataMahasiswa.setKodePos(mahasiswa.getKodePos());
    dataMahasiswa.setNomorHanphone(mahasiswa.getNomorHanphone());

    dataMahasiswa.setModifedBy(2L);
    dataMahasiswa.setModifedOn(new Date());
    return mhsRepo.save(dataMahasiswa);

  }

  @Override
  public Mahasiswa deleteMahasiswa(Long id) {
    Mahasiswa mahasiswa = mhsRepo.findById(id).get();

    mahasiswa.setIsDelete(true);
    mahasiswa.setDeletedBy(3L);
    mahasiswa.setDeletedOn(new Date());

    return mhsRepo.save(mahasiswa);
  }

  @Override
  public Page<Mahasiswa> getUsingPaging(Pageable pageable) {
    return mhsRepo.getPaging(pageable);
  }

}