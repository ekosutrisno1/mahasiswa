package com.crud.service.implement;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.crud.model.transaksi.DataSkripsi;
import com.crud.repository.DataSkripsiRepository;
import com.crud.service.DataSkripsiService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * DataSkripsiImpl
 */
@Service
public class DataSkripsiImpl implements DataSkripsiService {

  @Autowired
  private DataSkripsiRepository dataSkripsiRepo;

  @Override
  public Page<DataSkripsi> getAllPagingPage(Pageable pageable) {
    return dataSkripsiRepo.getAllPaging(pageable);
  }

  @Override
  public List<DataSkripsi> getAll() {
    List<DataSkripsi> dataSkripsi = new ArrayList<>();
    for (DataSkripsi data : dataSkripsiRepo.findAll()) {
      if (!data.getIsDelete()) {
        dataSkripsi.add(data);
      }
    }
    return dataSkripsi;
  }

  @Override
  public Optional<DataSkripsi> getById(Long id) {
    return dataSkripsiRepo.findById(id);
  }

  @Override
  public DataSkripsi save(DataSkripsi dataSkripsi) {
    dataSkripsi.setCreatedOn(new Date());
    dataSkripsi.setCreatedBy(3L);
    dataSkripsi.setTanggalMulai(new Date());
    return dataSkripsiRepo.save(dataSkripsi);
  }

  @Override
  public DataSkripsi update(DataSkripsi dataSkripsi) {
    DataSkripsi skripsi = dataSkripsiRepo.findById(dataSkripsi.getId()).get();

    skripsi.setId(dataSkripsi.getId());
    skripsi.setKodeMahasiswa(dataSkripsi.getKodeMahasiswa());
    skripsi.setJudulSkripsi(dataSkripsi.getJudulSkripsi());

    skripsi.setModifedBy(2L);
    skripsi.setModifedOn(new Date());
    return dataSkripsiRepo.save(skripsi);
  }

  @Override
  public DataSkripsi delete(Long id) {
    DataSkripsi skripsi = dataSkripsiRepo.findById(id).get();

    skripsi.setIsDelete(true);
    skripsi.setDeletedBy(1L);
    skripsi.setDeletedOn(new Date());

    return dataSkripsiRepo.save(skripsi);
  }

}