package com.crud.service.implement;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.crud.model.transaksi.DosenPembimbing;
import com.crud.repository.DosenPembimbingRepository;
import com.crud.service.DosenPembimbingService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * DosenPembimbingImpl
 */
@Service
public class DosenPembimbingImpl implements DosenPembimbingService {

  @Autowired
  private DosenPembimbingRepository dosenPembimbingRepository;

  @Override
  public Page<DosenPembimbing> getAllPagingPage(Pageable pageable) {
    return dosenPembimbingRepository.getAllPaging(pageable);
  }

  @Override
  public List<DosenPembimbing> getAll() {
    List<DosenPembimbing> dataDosen = new ArrayList<>();
    for (DosenPembimbing dosPem : dosenPembimbingRepository.findAll()) {
      if (!dosPem.getIsDelete()) {
        dataDosen.add(dosPem);
      }
    }
    return dataDosen;
  }

  @Override
  public Optional<DosenPembimbing> getById(Long id) {
    return dosenPembimbingRepository.findById(id);
  }

  @Override
  public DosenPembimbing save(DosenPembimbing dosenPembimbing) {
    dosenPembimbing.setCreatedBy(1L);
    dosenPembimbing.setCreatedOn(new Date());
    dosenPembimbing.setTanggalMulai(new Date());
    return dosenPembimbingRepository.save(dosenPembimbing);
  }

  @Override
  public DosenPembimbing update(DosenPembimbing dosenPembimbing) {
    DosenPembimbing dataDosen = dosenPembimbingRepository.findById(dosenPembimbing.getId()).get();

    dataDosen.setId(dosenPembimbing.getId());
    dataDosen.setKodeDosen(dosenPembimbing.getKodeDosen());
    dataDosen.setKodeMahasiswa(dosenPembimbing.getKodeMahasiswa());
    dataDosen.setTanggalMulai(dosenPembimbing.getTanggalMulai());

    dataDosen.setModifedBy(2L);
    dataDosen.setModifedOn(new Date());

    return dosenPembimbingRepository.save(dataDosen);
  }

  @Override
  public DosenPembimbing delete(Long id) {
    DosenPembimbing dosen = dosenPembimbingRepository.findById(id).get();

    dosen.setDeletedBy(1L);
    dosen.setDeletedOn(new Date());
    dosen.setIsDelete(true);

    return dosenPembimbingRepository.save(dosen);
  }

}