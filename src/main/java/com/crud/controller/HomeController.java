package com.crud.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * HomeController
 */
@Controller
public class HomeController {

  @RequestMapping("/")
  public String viewIndex() {
    return "index";
  }

  @RequestMapping("/mahasiswa")
  public String viewMahasiswaAPI() {
    return "views/mahasiswa";
  }

  @RequestMapping("/profil")
  public String viewProfil() {
    return "views/profil";
  }

  @RequestMapping("/dashboard")
  public String viewBeranda() {
    return "views/beranda";
  }

  @RequestMapping("/ro")
  public String viewRole() {
    return "views/role";
  }
}