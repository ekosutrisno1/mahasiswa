package com.crud.repository;

import com.crud.model.master.Ujian;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UjianRepository extends JpaRepository<Ujian, Long> {
}
