# OC-ExoApp V.1.0
>>>
This is an Exercise Project to make an online course using the LARAVEL framework. This project starts on March 27, 2020.

Author: Eko Sutrisno | Trainer : Saroyo Hendro Wibowo
>>>
---

## Konten dan Info

-   [Installation](#installation)
-   [Usage](#usage)
-   [Author](#author)
-   [License](#license)

---

## Installation
>>>
**Tip:**
Pastikan komputer kamu sudah terinstall composer.

-   Download atau clone.
-   Kemudian buka terminal / cmd dan arahkan ke folder project yang sudah kamu download/clone.
-   kemudian buat database dengan nama `onlinecourse` kemudian atur username dan password mysql kamu.
-   Buka file .env kemudian atur properti database kamu.
-   Buka folder config dan buka file Database.php kemudian di properti mysql atur juga username dan passwordnya sesuai dengan yang di file .env tadi.
>>>
##Usage

`php artisan migrate`

`composer dump-autoload`

`php artisan migrate:fresh --seed`

`php artisan serve`

## Author

Eko Sutrisno

## License

[MIT](https://choosealicense.com/licenses/mit/)
